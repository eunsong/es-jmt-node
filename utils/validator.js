
const validator = require('@hapi/joi');



// const inputValidator = (data)=>{
//     const schema = validator.object({
//         placeName : validator.string().min(2).required().trim(),
//         category_code : validator.string().alphanum().max(3).uppercase().trim()
//     })
//     return schema.validate(data);
// }

// module.exports.inputValidator = inputValidator;


module.exports = class inputValidator{

    kakaoValidator(data){
        const schema = validator.object({
            placeName : validator.string().min(2).required().trim()
        })
        return schema.validate(data);
    }

    addDataValidator(data){
        const schema = validator.object({
            jmtName : validator.string().min(2).required(),
            jmtAddress : validator.string().required(),
            jmtDate : validator.string().required(),
            jmtLocation : validator.string().allow(''),
            jmtLocationKey : validator.string().allow(''),
            jmtPrice : validator.number().allow(''),
            jmtCategory: validator.string().allow(''),
            jmtCategoryKey: validator.string().allow(''),
            jmtRefer : validator.string().allow(''),
            jmtRating : validator.number().allow(''),
            map: {
                x : validator.string().allow(''),
                y : validator.string().allow('')
            },
            list: validator.array().allow('')
        })
        return schema.validate(data);
    }

    updateDataValidator(data){
        const schema = validator.object({
            id : validator.string().required(),
            data :{
                jmtDate : validator.string().required(),
                jmtLocation : validator.string().allow(''),
                jmtLocationKey : validator.string().allow(''),
                jmtPrice : validator.number().allow(''),
                jmtCategory: validator.string().allow(''),
                jmtCategoryKey: validator.string().allow(''),
                jmtRefer : validator.string().allow(''),
                jmtRating : validator.number().allow(''),
                list: validator.array().allow('')
            }
        })
        return schema.validate(data);
    }

    deleteDataValidator(data){
        const schema = validator.object({
            id : validator.string().required()
        })
        return schema.validate(data);
    }

    downloadFileValidator(data){
        const schema = validator.object({
            fileName : validator.string().required()
        })
        return schema.validate(data);
    }

}