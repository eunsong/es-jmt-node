
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());

const kRouter = require('./routes/kakao.router');
const firebaseRouter = require('./routes/firebase.router')
const ISTN_API_VERSION = 'v1'

app.use(`/api/${ISTN_API_VERSION}`, firebaseRouter);

//app.use()는 node에 미들웨어 등록하는거 --> "/api/${ISTN_API_VERSION}"으로 들어오면 실제 routing은 /api/vi/***으로 간다
app.use(`/api/${ISTN_API_VERSION}`, kRouter);

const PORT = process.env.PORT || 3000;
app.listen(PORT, ()=>{ console.log(`Server is running with ${PORT}`)});