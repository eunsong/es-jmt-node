
const router = require('express').Router();

const FirebaseService = require('../services/firebaseServices.service');
const firebaseServices = new FirebaseService();

const Validator = require('../utils/validator');
const validator = new Validator();

const multer = require('multer');
const myUploader = multer();

const path = require('path');
const fs = require('fs');

router.get('/jmt', async (req, res)=>{ 
    var result = await firebaseServices.getJmtData();
    if(result.status === "success"){
        res.send(result);
    }else{
        res.status(400).send(result.msg);
    }
})


router.post('/jmt', async (req, res)=>{    //jmt
    const { error } = validator.addDataValidator(req.body);
    if(error) return res.status(400).send(error.details[0].message); 

    var result = await firebaseServices.addJmtData(req.body);
    if(result.status === "success"){
        res.send(result);
    }else{
        res.status(400).send(result.msg);
    }
})

router.put('/jmt', async (req, res)=>{
    const { error } = validator.updateDataValidator(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    var result = await firebaseServices.updateJmtData(req.body.id, req.body.data);
    if(result.status === "success"){
        res.send(result);
    }else{
        res.status(400).send(result.msg);
    }
})

router.delete('/jmt', async (req, res)=>{
    const { error } = validator.deleteDataValidator(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    var result = await firebaseServices.deleteJmtData(req.body.id);
    if(result.status === "success"){
        res.send(result);
    }else{
        res.status(400).send(result.msg);
    }
})

// router.post('/upload', myUploader.single('myJMT'), async (req, res) => {
router.post('/upload', myUploader.array('myJMT', 3), async (req, res) => {
    let files = req.files;
    if (files.length > 0) {
        var promises = files.map(async (file)=>{
            var result = await firebaseServices.uploadImageToStorage(file);
            return {
                name : file.originalname,
                url : result.data
            }
        })
        var urlList = await Promise.all(promises);
        res.send(urlList);
    }else{
      res.status(400).send('업로드 할 파일이 존재하지 않습니다.');
    }
});

router.get('/jmt/codeList', (req, res)=>{
    var codes = firebaseServices.getCodes();
    res.json(codes)
})

router.post('/download', async (req, res)=>{
    // const { error } = validator.downloadFileValidator(req.body);
    // if(error) return res.status(400).send(error.details[0].message);

    // var result = await firebaseServices.downloadFile(req.body.fileName);
    // console.log(result)

    // // var test = result.toString('binary');
    // // console.log(test)

    // res.send(new Buffer.from(result).toString('base64'))
    // // if(result.status !== "error"){
    // //     res.setHeader( 'Content-Disposition', 'attachment; filename=' + req.body.fileName );
    // //     res.setHeader( 'Content-Type', 'image/png;' );
        
    // //     res.end(result);
    // // }else{
    // //     res.status(400).send(result);
    // // }
})


module.exports = router;