
const router = require('express').Router();
const KakaoService = require('../services/KakaoServices.service');
const kakaoService = new KakaoService();    //인스턴스화
const Validator = require('../utils/validator');
const validator = new Validator();

router.post('/place', async (req, res)=>{
    const { error } = validator.kakaoValidator(req.body); // validation
    if(error) return res.status(400).send(error.details[0].message);

    var results = await kakaoService.searchKeyword(req.body.placeName);
    if(results.status === "sucess"){
        var data = results.data.documents.map(item=>{
            return {
                palceId: item.id,
                placeName : item.place_name,
                map: {
                    "x": item.x,
                    "y": item.y
                },
                category_name : item.category_name,
                detailLink : item.place_url,
                road_address_name : item.road_address_name,
                address : item.address_name,
            }
        });
        var nData = {
            metaData : results.data.meta,
            mapData : data
        };
        if(nData.metaData.total_count === 0){
            res.send("검색하신 결과가 없습니다.")            
        }else{
            res.send({ data: nData });
        };
    }else{
        return res.status(400).send("현재 서비스가 원활하지 않습니다. 다시 시도해주세요");
    }
});
router.get('/category/code', (req, res)=>{
    var result = kakaoService.getCategoryCode();
    if(result.status === "sucess"){
        res.json(result.data);
    }else{
        return res.status(400).send("현재 서비스가 원활하지 않습니다. 다시 시도해주세요");
    }
})

module.exports = router;