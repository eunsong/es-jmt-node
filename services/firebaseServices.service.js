
const request = require('request');
const config = require('../configs/config');
var fireBase = require('firebase-admin');

fireBase.initializeApp({
    credential: fireBase.credential.cert(config.firebaseAdminConfig),
    databaseURL: 'https://jmt-project-52a01.firebaseio.com'
})

const COLLECTION_NAME = "jmt";
var myDatabase = fireBase.firestore().collection(COLLECTION_NAME);

const { Storage } = require('@google-cloud/storage');
const { format } = require('util');
const storage = new Storage({keyFilename: './configs/jmt-project-52a01-firebase-adminsdk-gpabw-3503612936.json', 'projectId': 'jmt-project-52a01'})
const bucket = storage.bucket("gs://jmt-project-52a01.appspot.com");

const path = require('path');
const fs = require('fs');

module.exports = class firebaseService{

    getJmtData(){
        return new Promise((resolve, reject)=>{
            myDatabase
            .orderBy('jmtDate', 'desc')
            .get()
            .then(data=>{
                // console.log(data)
                var jmtData = [];
                data.forEach((doc)=>{
                    jmtData.push(Object.assign({id: doc.id}, doc.data()));
                })
                resolve({
                    "status" : "success",
                    "data" : jmtData
                });
            })
            .catch(error=>{
                reject({
                    "status" : "error",
                    "msg" : error
                });
            })
        })
    }

    addJmtData(data){
        return new Promise((resolve, reject)=>{
            myDatabase.add(data)
            .then(data=>{
                resolve({
                    "status" : "success",
                    "data" : `${data._path.segments[1]}`
                });
            })
            .catch(error=>{
                reject({
                    "status" : "error",
                    "msg" : error
                });
            })
        })
    }
    
    //deleteJmtData랑 합쳐보기
    updateJmtData(id, data){
        return new Promise((resolve, reject)=>{
            myDatabase.doc(id).update(data)
            .then(data=>{
                resolve({
                    "status" : "success",
                    "data" : '정상적으로 수정되었습니다.'
                });
            })
            .catch(error=>{
                reject({
                    "status" : "error",
                    "msg" : error
                });
            })
        })
    }


    deleteJmtData(data){
        return new Promise((resolve, reject)=>{
            myDatabase.doc(data).delete()
            .then(data=>{
                resolve({
                    "status" : "success",
                    "data" : '정상적으로 삭제되었습니다.'
                });
            })
            .catch(error=>{
                reject({
                    "status" : "error",
                    "msg" : error
                });
            })
        })
    }

    uploadImageToStorage(file){
        return new Promise((resolve, reject)=>{
            if (!file) {
                reject({
                    "status": "error",
                    "msg" : 'No image file'
                });
              }
                let newFileName = file.originalname + "--" + Date.now();
                let fileUpload = bucket.file(newFileName);
                
                const blobStream = fileUpload.createWriteStream({
                    metadata: {
                    contentType: file.mimetype
                    }
                });
            
                blobStream.on('error', (error) => {
                    reject({
                        "status" : "error",
                        "msg" : 'Something is wrong! Unable to upload at the moment.'
                    });
                });
            
                blobStream.on('finish', ()=> {
                    const url = format(`https://storage.googleapis.com/${bucket.name}/${fileUpload.name}`);
                    resolve({
                        "status" : "success",
                        "data" : url
                    });
                });
            
                blobStream.end(file.buffer);
              })
    }

    downloadFile(fileName){
        return new Promise((resolve, reject)=>{

            bucket.file(fileName).download({})
                .then(data=>{
                    const contents = data[0];
                    resolve(contents)
                    // resolve({
                    //     status : "success",
                    //     data : contents
                    // })
                })
                .catch(error=>{
                    reject({
                        status : "error",
                        msg : error
                    })
                });
        })
    }

    getCodes(){
        try {
            var categories = fs.readFileSync(path.join('models', 'categories.json'), 'utf-8'); 
            var locations = fs.readFileSync(path.join('models', 'location.json'), 'utf-8'); 
            if(typeof categories === 'string') categories = JSON.parse(categories);
            if(typeof locations === 'string') locations = JSON.parse(locations);
            console.log(categories)
            console.log(locations)
            var codeList = {
                "categories" : categories,
                "locations" : locations,
            };
            return codeList;
        } catch (error) {
            return error;
        }
    }

}