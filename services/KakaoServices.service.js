
const request = require('request');
const fs = require('fs');
const path = require('path');
const config = require('../configs/config');
const KAKAO_URL = 'https://dapi.kakao.com';

module.exports = class KakaoService{

    searchKeyword(placeName){
        return new Promise((resolve, reject)=>{
            request({
                url: `${KAKAO_URL}/v2/local/search/keyword.json`,
                headers: { Authorization : `KakaoAK ${config.kakaoAPI.RESTAPI_KEY[1]}` },
                qs : { query : placeName }
            }, (err, res, body)=>{
                if(err) return reject({ status : "error", msg : err});
                resolve({ status: "sucess", data: JSON.parse(body)});
            })
        })
    }
    getCategoryCode(){
        try {
            var data = fs.readFileSync(path.join('models/kakao', 'category_code.json'), 'utf-8');  //path module필요-github가보기
            if(data){
                if(typeof data === 'string') data = JSON.parse(data);
                var codeList = [];
                Object.keys(data).forEach(code=>{
                    codeList.push({ code : code, code_text : data[code] })
                })
                codeList.splice(0, 0, {code: "ALL", code_text: "전체"})
                return { status: "sucess", data: codeList};
            }else{
                return { status: "error", msg : "카테고리가 없습니다."};
            }
        } catch (error) {
            return { status: "error", msg : error};
        }
    }
}